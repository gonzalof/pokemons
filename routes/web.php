<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home / index
Route::get('/', 'HomeController@index');

// Pokemon detalis
Route::get('/pokemon/{name}', 'HomeController@show');

// Pokemon finder by partial name endpoint
Route::get('/find', 'PokemonsController@findByName');


