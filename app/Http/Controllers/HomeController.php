<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;


use App\Pokemon;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $random_pokemons = Pokemon::all()->random(6);

        return view('welcome', [
            'random_pokemons' => $random_pokemons
            ]);
    }

    /**
     * Display the specified pokemon.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $client = new Client();

        try {
            $response = $client->get(env('POKEMON_API_URL').'/pokemon/'.$name.'/');
            $result = json_decode($response->getBody());
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            abort(404);
        }
        return view('pokemon', [
            'pokemon' => $result
        ] );
    }
}
