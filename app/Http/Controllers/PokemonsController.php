<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pokemon;

class PokemonsController extends Controller
{
    /**
     * Find pokemons in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function findByName(Request $request)
    {
        if($request->term == '') return;

        $pokemons = Pokemon::where('name', 'LIKE', '%'.$request->term.'%')->get();
        return response()->json($pokemons, 200);
    }


}
