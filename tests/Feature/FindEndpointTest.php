<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FindEndpointTest extends TestCase
{
    /**
     * Find Pokemon test.
     *
     * @return void
     */
    public function testFindPokemon()
    {
        $response = $this->get('/find?term=pidgey');

        $response->assertStatus(200);
    }
}
