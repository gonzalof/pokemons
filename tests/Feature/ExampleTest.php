<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * Home test.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/')
            ->assertStatus(200);

    }

    /**
     * Pokemon Details test.
     *
     * @return void
     */
    public function testPokemonDetailsUrl()
    {
        $response = $this->get('/pokemon/pikachu')
            ->assertStatus(200);

    }
}
