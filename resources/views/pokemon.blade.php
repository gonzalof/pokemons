@extends('layouts.app')

@section('content')

@include('finder')
<h4>{{ ucfirst($pokemon->name) }}</h4>
<div class="row">
    <div class="col-sm-4">
            <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/{{ $pokemon->id }}.png" 
            class="card-img-top" 
            alt="..."
            onerror="this.src = '{{url('/')}}/imgs/pokemon_logo.png';">
    </div>

    <div class="col-sm-8">
        <div class="container rounded bg-light">
            <div class="row">
                <div class="col-sm-4 p-2">
                    <h5>Height</h5>
                    {{ ($pokemon->height)/10 }} metres
                </div>
                <div class="col-sm-4 p-2">
                    <h5>Weight</h5>
                    {{ ($pokemon->weight)/10 }} Kgrams
                </div>
                <div class="col-sm-4 p-2">
                    <h5>species</h5>
                    {{ $pokemon->species->name }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 p-2">
                    <h5>Abilities</h5>
                    <ul class="list-group">
                        @foreach ($pokemon->abilities as $ability)
                            <li class="list-group-item">{{ $ability->ability->name }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-6 p-2">
                    <h5>Types</h5>
                    <ul class="list-group">
                        @foreach ($pokemon->types as $type)
                            <li class="list-group-item">{{ $type->type->name }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <h3>Stats</h3>
    <table class="table">
    <tbody>
        @foreach ($pokemon->stats as $stat)               
            <tr>
                <td style="width: 20%">{{ $stat->stat->name }}</td>
                <td>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped bg-danger" role="progressbar" style="width: {{ $stat->base_stat }}%" aria-valuenow="{{ $stat->base_stat }}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>
</div>
<hr>
<div class="row">
    <div class="col-sm-6 p-2">
    <a class="btn btn-outline-primary" href="{{ env('APP_URL') }}"><i class="fa fa-reply"></i> Go back</a>
    </div>
</div>
@endsection