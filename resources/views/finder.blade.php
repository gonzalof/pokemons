<div class="row">
    <div class="col-sm-6 offset-sm-3">
        <img src="{{url('/')}}/imgs/pokemon_logo_title.png" style="width: 100%">
    </div>
</div>
<div class="row mt-4">
    <div class="col-sm-6 offset-sm-3">
        <div class="row">
            <div class="col-sm-9">
                <select class="js-example-basic-single js-states form-control" id="id_select2_pokemon"></select>
            </div>
            <div class="col-sm-3">
                <button class="btn btn-outline-primary" id="find_button"><i class="fa fa-search" aria-hidden="true"></i> Find</button>
            </div>
        </div>
    </div>
</div>
<hr>