@extends('layouts.app')

@section('content')

@include('finder')

<h4>Some pokemons</h4>
<div class="card-columns">
    @foreach ($random_pokemons as $pokemon)
        <div class="card">
            <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/{{ $pokemon->id }}.png" 
                class="card-img-top" 
                alt="..."
                onerror="this.src = 'imgs/pokemon_logo.png';">
            <div class="card-body">
                <h5 class="card-title">{{ ucfirst($pokemon->name) }}</h5>
            <a class="btn btn-outline-primary btn-block" href="{{ env('APP_URL') }}/pokemon/{{ $pokemon->name }}"><i class="fa fa-arrow-circle-right"></i> View More</a>
            </div>
        </div>
    @endforeach
</div>
@endsection