# ![Laravel Pokemon App](https://i.pinimg.com/originals/d8/43/ad/d843addbfcec31846d8699feebcf358b.png)

# Getting started 

## Requirements

PHP >= 7.1.3

pdo_sqlite PHP Extension

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.4/installation#installation)



Clone the repository

    git clone https://bitbucket.org/gonzalof/pokemons.git

Switch to the repo folder

    cd pokemons

Install all the dependencies using composer

    composer install

Generate the sqlite file for database in database folder

    eg: touch database/database.sqlite

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Configure the env file with the app url and the sqlite database file

    eg: APP_URL=http://localhost/pokemons/public (without artisan serve)
        APP_URL=http://127.0.0.1:8000 (with artisan serve)
        DB_DATABASE=/apache2/htdocs/pokemons/database/database.sqlite

Set the default DB_CONNECTION to sqlite in config/database.php

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

## Database seeding

**Populate the database with seed the pokemons data **

    php artisan db:seed

----------

## Start the local development server ( or browser to your proyect public folder )

    php artisan serve

----------

## Unit Testing

Copy the env file and make the required configuration changes in the .env.testing file

    cp .env .env.testing

Change APP_ENV and APP_URL to:
    
    APP_ENV=test
    APP_URL=http://localhost

Run the tests

    vendor/bin/phpunit

----------

# Code overview

## Folders

- `app` - Contains all the Eloquent models
- `app/Http/Controllers/` - Contains all the controllers
- `config` - Contains all the application configuration files
- `database/migrations` - Contains all the database migrations
- `database/seeds` - Contains the database seeder
- `resources/views` - Contains the views
- `routes` - Contains all the routes defined in web.php file
- `tests` - Contains all the application tests

## Environment variables

- `.env` - Environment variables can be set in this file

***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.
