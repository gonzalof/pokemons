<?php

use Illuminate\Database\Seeder;

use GuzzleHttp\Client;

class PokemonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = self::data();

        DB::table('pokemons')->delete();

        foreach($data as $value){

            $search = env('POKEMON_API_URL').'/pokemon/';

            $pokemon['id'] = substr(str_replace($search,"",$value->url), 0, -1);
            $pokemon['name'] = $value->name;
            $pokemon['url'] = $value->url;

            DB::table('pokemons')->insert($pokemon);
        }
    }

    public static function data() {
    
        $client = new GuzzleHttp\Client();
        $res = $client->get(env('POKEMON_API_URL').'/pokemon');
        
        if ($res->getStatusCode() == 200 ){
            $result = json_decode($res->getBody());
            $total = $result->count;
        } else {
            return;
        }

        $res = $client->get(env('POKEMON_API_URL').'/pokemon?offset=0&limit='.$total);
        
        if ($res->getStatusCode() == 200 ){
            $result = json_decode($res->getBody());
            $pokemons = $result->results;
        } else {
            return;
        }

        return $pokemons;
    }

}
